#include <iostream>
#include <string>
#include "Vector.h"
using namespace std;


void Vector::pr() // Function prints the element list
{
	for (int i = 0; i < this->_size; i++)
	{
		printf("%d.", i);
		cout << this->_elements[i];
		cout << '\n';
	}
}

Vector::Vector(int n)
{
	if (n<2)
	{
		n = 2;
	}
	this->_elements = new int[n];
	this->_resizeFactor = n;
	this->_size = 0;
	this->_capacity = n;
}
 
int Vector::size() const 
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return this->_size == 0;
}

Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
}

void Vector::push_back(const int& val) 
{
	if (this->_size +1 ==this->_capacity) // If size is almost full
	{
		cout << "[*] Allocating new space... Size is " << this->_size << " Cap is " << this->_capacity<< '\n'<<endl;
		int*  newElements = new int[this->_capacity + this->_resizeFactor]; // allocate new elements array at the size of  capacity and resize factor
		for (int  x = 0; x < this->_size; x++) // Copy current values to the new array
		{ 
			newElements[x] = this->_elements[x]; // Copy elements
		}
		delete[] this->_elements; // Delete old array
		this->_elements = nullptr; // Secure memory
		this->_elements = newElements; // Reasign new elements
		this->_capacity += this->_resizeFactor; // Expand the capacity
		cout << "[/] Cap --update " << this->_capacity <<'\n' <<endl;
	}
	
	this->_elements[this->_size] = val; // 
	this->_size++;
}

int Vector::pop_back()
{
	
	if(!this->empty())
	{
		this->_size--; 
		return this->_elements[this->_size];
	}
	cout << "error: pop from empty vector";
	return -9999;
}

void Vector::reserve(int n)
{
	if (n> this->_resizeFactor)
	{
		int*  newElements; 
		int oldCap = this->_capacity;
		if(n%this->_resizeFactor == 0) // If n is divided by resize factor with no remainder 
		{ 
			cout << "[=] Allocating cap: " << this->_capacity << "+" << (this->_resizeFactor*(n / this->_resizeFactor));
			newElements = new int[this->_capacity + (this->_resizeFactor*(n / this->_resizeFactor))]; // Allocate cap + (factor)*(n/factor) cap = 10 | n = 15  |factor 5 ||| >> 10 + 5*(3) >> 25	
			this->_capacity += (this->_resizeFactor*(n / this->_resizeFactor)); // Update current capacity
		}
		else
		{
			cout << "[=] Allocating cap: " << this->_capacity << "+" << (this->_resizeFactor*((n / this->_resizeFactor) + 1));
			newElements = new int[this->_capacity + (this->_resizeFactor*((n / this->_resizeFactor) +1))]; // Allocate cap + (factor)*((n/factor)+1) cap = 10 | n = 17  |factor 5 ||| 10 + 5*(3+1) >>> 30
			this->_capacity += (this->_resizeFactor*((n / this->_resizeFactor) + 1)); // Update current capacity
		}
		for (int i = 0; i < oldCap; i++) // Update new array
		{
			newElements[i] = this->_elements[i];
		}
		delete[] this->_elements; 
		this->_elements = nullptr;
		this->_elements = newElements; 
	}
	else // Raise a warning
	{
		cout << "[!] Can't reserve becuase n->" << n << " is small then resize factor " << this->_resizeFactor <<endl;
	}
}

void Vector::resize(int n)
{
	if (n<=this->_capacity)
	{
		int* newElements = new int[n];
		for (int i = 0;  i < n;  i++)
		{
			newElements[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = nullptr; 
		this->_elements = newElements;
		this->_capacity = n;
		this->_size =  n;
	}
	else
	{
		int oldSize = this->_size;
		this->_size = n;
		reserve(n); // Add new place
		for (int i = oldSize-1; i < this->_size; i++)
		{
			this->_elements[i] = 0; // Reset new added values
		}
	}
}

void Vector::resize(int n, const int& val)
{
	if (n <= this->_capacity) 
	{
		int* newElements = new int[n];
		for (int i = 0; i < n; i++) 
		{
			newElements[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = nullptr;
		this->_elements = newElements;
		this->_capacity = n;
		this->_size = n;
	}
	else
	{
		int oldSize = this->_size; // Save old size
		this->_size = n;
		reserve(n); // Add new place
		for (int i = oldSize - 1; i < this->_size; i++)
		{
			this->_elements[i] = val; // Reset new added values
		}
	}
	
	
}

void Vector::assign(int val)
{
	for (int i = 0; i < this->_capacity; i++)
	{
		this->_elements[i] = val;
	}
}

Vector::Vector(const Vector& other)
{
	//*this = other;
	if (this->_elements)
	{
		delete[] this->_elements;
		this->_elements = nullptr;
	}


	int * newElements = new int[other._size];
	for (int i = 0; i < other._size; i++)
	{
		// copies cell by cell
		newElements[i] = other._elements[i];
	}
	this->_elements = newElements;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	this->_capacity = other._capacity;

}

Vector& Vector::operator=(const Vector& other)
{
	if (this->_elements)
	{
		delete[] this->_elements;
	}
	return *(new Vector(other));
}

int& Vector::operator[](int n) const
{
	if (!empty()) // Check end states
	{
		if (n >= this->_size)
		{
			cout << "[!] N is outside the boundaries of the array\n";
			return this->_elements[0];
		}return this->_elements[n];
	}
	
}

void  Vector::operator+=(const Vector& other)
{	
	int sizeOfBigger = 0;
	if (this->_size > other._size)
	{
		for (int i = 0; i < other._size; i++)
		{
			this->_elements[i] += other._elements[i];
		}
	}
	else
	{
		int newSize = this->_size + (other._size - this->_size);
		int *newElements = new int[newSize]; // Allocate some new space for bigger vector
		for (int i = 0; i < this->_size; i++)
		{
			newElements[i] = this->_elements[i];
		}
		for (int i = this->_size; i < newSize; i++)
		{
			newElements[i] = 0;
		}
		delete[] this->_elements;
		this->_elements = nullptr;
		this->_size = other._size;
		this->_capacity = other._capacity;
		this->_elements = newElements;
		for (int i = 0; i < this->_size; i++)
		{
			this->_elements[i] += other._elements[i];
		}
	}
}

void Vector::operator-=(const Vector& other)
{
	int sizeOfBigger = 0;
	if (this->_size > other._size)
	{
		for (int i = 0; i < other._size; i++)
		{
			this->_elements[i] -= other._elements[i];
		}
	}
	else
	{
		int newSize = this->_size + (other._size - this->_size);
		int *newElements = new int[newSize]; // Allocate some new space for bigger vector
		for (int i = 0; i < this->_size; i++)
		{
			newElements[i] = this->_elements[i];
		}
		for (int i = this->_size; i < newSize; i++)
		{
			newElements[i] = 0;
		}
		delete[] this->_elements;
		this->_elements = nullptr;
		this->_size = other._size;
		this->_capacity = other._capacity;
		this->_elements = newElements;
		for (int i = 0; i < this->_size; i++)
		{
			this->_elements[i] -= other._elements[i];
		}
	}
} 

